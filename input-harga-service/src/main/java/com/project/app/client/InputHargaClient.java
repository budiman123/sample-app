package com.project.app.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.project.app.custom.harga.InputHargaRequest;
import com.project.app.custom.harga.InputHargaResponse;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@Service
public class InputHargaClient {

	@Value("${input-harga.url}")
	private String URL;
	private final ObjectMapper objectMapper = new ObjectMapper()
			.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

	public InputHargaResponse inputHarga(InputHargaRequest inputHargaRequest) throws Exception {
		OkHttpClient client = new OkHttpClient().newBuilder().build();
		MediaType mediaType = MediaType.parse("application/json");
		Gson gson = new Gson();
		String json = gson.toJson(inputHargaRequest);
		RequestBody body = RequestBody.create(mediaType, json);
		Request request = new Request.Builder().url(URL).method("POST", body)
				.addHeader("Content-Type", "application/json").build();
		Response response = client.newCall(request).execute();

		InputHargaResponse inputHargaResponse = objectMapper.readValue(response.body().string(),
				InputHargaResponse.class);
		if (inputHargaResponse.getError()) {
			throw new Exception(inputHargaResponse.getMessage());
		}
		return inputHargaResponse;
	}
}
