package com.project.app.service;

import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.app.client.InputHargaClient;
import com.project.app.custom.harga.InputHargaRequest;
import com.project.app.custom.harga.InputHargaResponse;

@Service
public class TblHargaService {

	private final InputHargaClient inputHargaClient;

	@Autowired
	public TblHargaService(InputHargaClient inputHargaClient) {
		this.inputHargaClient = inputHargaClient;
	}

	public InputHargaResponse doInputHarga(InputHargaRequest inputHargaRequest) {
		InputHargaResponse response = new InputHargaResponse();
		response.setReffId(UUID.randomUUID().toString());
		response.setError(false);
		try {
			response = inputHargaClient.inputHarga(inputHargaRequest);
			return response;
		} catch (Exception e) {
			response.setError(true);
			response.setMessage(e.getMessage());
			return response;
		}
	}
}
