
package com.project.app.custom.harga;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter 
@NoArgsConstructor
public class InputHargaRequest {
	@JsonProperty("admin_id")
	@SerializedName("admin_id")
	@NotNull
	private String adminId;
		
	@JsonProperty("harga_topup")
	@SerializedName("harga_topup")
	@NotNull
	private Long hargaTopup;
	
	@JsonProperty("harga_buyback")
	@SerializedName("harga_buyback")
	@NotNull
	private Long hargaBuyback;
}
