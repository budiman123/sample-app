package com.project.app.client;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.app.custom.harga.CekHargaResponse;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@Service
public class CheckHargaClient {

	@Value("${check-harga.url}")
	private String URL;
	private final ObjectMapper objectMapper = new ObjectMapper().disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

	public CekHargaResponse checkHarga() throws Exception {
		OkHttpClient client = new OkHttpClient().newBuilder().build();
		Request request = new Request.Builder().url(URL).method("GET", null).build();
		Response response = client.newCall(request).execute();
		CekHargaResponse cekHargaResponse = objectMapper.readValue(response.body().string(), CekHargaResponse.class);
		if(cekHargaResponse.getError()) {
			throw new Exception(cekHargaResponse.getMessage());
		}
		return cekHargaResponse;
	}
}
