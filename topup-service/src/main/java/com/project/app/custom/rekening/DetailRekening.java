
package com.project.app.custom.rekening;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter 
@NoArgsConstructor
public class DetailRekening {
	@JsonProperty("saldo")
	private BigDecimal saldo;
	
	@JsonProperty("norek")
	private String norek;
}
