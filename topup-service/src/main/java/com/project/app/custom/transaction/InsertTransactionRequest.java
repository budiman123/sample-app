
package com.project.app.custom.transaction;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class InsertTransactionRequest {
    private String type;
    private BigDecimal gram;
    private Long hargaTopup;
    private Long hargaBuyback;
    private BigDecimal saldo;
    private String norek;
    private Long timestamp;
}
