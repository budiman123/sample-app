
package com.project.app.custom.transaction;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class TransactionRequest {
	@JsonProperty("gram")
	@NotNull
	private String gram;

	@JsonProperty("harga")
	@NotNull
	private Long harga;

	@JsonProperty("norek")
	@NotNull
	private String norek;

	private String type;
}
