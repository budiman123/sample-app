
package com.project.app.custom.transaction;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class SaldoData {
	@JsonProperty("norek")
	private String norek;

	@JsonProperty("saldo")
	private BigDecimal saldo;

}
