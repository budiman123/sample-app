package com.project.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class TopupServiceApplication {

	public static void main(final String[] args) {
		SpringApplication.run(TopupServiceApplication.class, args);
	}

}
