package com.project.app.service;

import java.math.BigDecimal;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.project.app.client.CheckHargaClient;
import com.project.app.client.TopupClient;
import com.project.app.custom.harga.CekHargaResponse;
import com.project.app.custom.transaction.InsertTransactionRequest;
import com.project.app.custom.transaction.TransactionRequest;
import com.project.app.custom.transaction.TransactionResponse;

@Service
public class TblTransaksiService {
	private final ObjectMapper objectMapper = new ObjectMapper()
			.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

	private final Gson gson = new Gson();
	private final CheckHargaClient checkHargaClient;
	private final TopupClient topupClient;

	@Value("${topup.url}")
	private String TOPUP_URL;

	@Value("${check-saldo.url}")
	private String CHECK_SALDO_URL;

	@Autowired
	public TblTransaksiService(CheckHargaClient checkHargaClient, TopupClient topupClient) {
		this.checkHargaClient = checkHargaClient;
		this.topupClient = topupClient;
	}

	public TransactionResponse doTransaction(TransactionRequest transactionRequest) {
		TransactionResponse response = new TransactionResponse();
		response.setReffId(UUID.randomUUID().toString());
		response.setError(false);
		try {
			CekHargaResponse cekHargaResponse = validateTopup(transactionRequest.getGram(),
					transactionRequest.getHarga());

			InsertTransactionRequest insertTransactionRequest = new InsertTransactionRequest();
			insertTransactionRequest.setGram(new BigDecimal(transactionRequest.getGram()));
			insertTransactionRequest.setHargaBuyback(cekHargaResponse.getData().getHargaBuyback());
			insertTransactionRequest.setHargaTopup(cekHargaResponse.getData().getHargaTopup());
			insertTransactionRequest.setNorek(transactionRequest.getNorek());
			insertTransactionRequest.setTimestamp(System.currentTimeMillis());
			insertTransactionRequest.setType(transactionRequest.getType());
			String result = topupClient.doHttpPost(gson.toJson(insertTransactionRequest), TOPUP_URL);
			TransactionResponse transactionResponse = objectMapper.readValue(result, TransactionResponse.class);
			if (transactionResponse.getError()) {
				throw new Exception(transactionResponse.getMessage());
			}
			return response;
		} catch (Exception e) {
			response.setError(true);
			response.setMessage(e.getMessage());
			return response;
		}
	}

	private CekHargaResponse validateTopup(String gram, Long harga) throws Exception {
		// Check if gram is decimal and 3 digits
		if (gram.toString().matches("^\\d+\\.\\d+")) {
			if (!gram.toString().matches("^\\d{1}(\\.\\d{3})$")) {
				throw new Exception("Gram must be 0.001 multiplier");
			}
		}

		// find current harga
		CekHargaResponse cekHargaResponse = checkHargaClient.checkHarga();
		// if present then set current status to false

		if (cekHargaResponse.getData().getHargaTopup() > harga && cekHargaResponse.getData().getHargaTopup() < harga) {
			throw new Exception("Harga topup do not match");
		}
		return cekHargaResponse;
	}
}
