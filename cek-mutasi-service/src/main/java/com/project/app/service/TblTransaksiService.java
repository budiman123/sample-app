package com.project.app.service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.app.custom.transaksi.MutasiData;
import com.project.app.custom.transaksi.MutasiRequest;
import com.project.app.custom.transaksi.MutasiResponse;
import com.project.app.domain.TblTransaksi;
import com.project.app.repos.TblTransaksiRepository;

@Service
public class TblTransaksiService {

	private final TblTransaksiRepository tblTransaksiRepository;

	@Autowired
	public TblTransaksiService(final TblTransaksiRepository tblTransaksiRepository) {
		this.tblTransaksiRepository = tblTransaksiRepository;
	}

	public MutasiResponse getMutasi(MutasiRequest mutasiRequest) {
		MutasiResponse response = new MutasiResponse();
		response.setReffId(UUID.randomUUID().toString());
		response.setError(false);
		try {
			List<TblTransaksi> listTransaksi = tblTransaksiRepository.getMutasiBetweenDateAndNorek(
					mutasiRequest.getStartDate(), mutasiRequest.getEndDate(), mutasiRequest.getNorek());
			List<MutasiData> listResult = listTransaksi.stream().map(transaksi -> {
				MutasiData data = new MutasiData();
				data.setDate(transaksi.getTimestamp());
				data.setGram(transaksi.getGram());
				data.setHargaBuyback(transaksi.getHargaBuyback());
				data.setHargaTopup(transaksi.getHargaTopup());
				data.setSaldo(transaksi.getSaldo());
				data.setType(transaksi.getType());
				return data;
			}).collect(Collectors.toList());
			response.setData(listResult);
			return response;
		} catch (Exception e) {
			response.setError(true);
			response.setMessage(e.getMessage());
			return response;
		}
	}
}
