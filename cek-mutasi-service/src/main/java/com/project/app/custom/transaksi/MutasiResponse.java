
package com.project.app.custom.transaksi;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class MutasiResponse {
	@JsonProperty("error")
	private Boolean error;

	@JsonProperty("reff_id")
	private String reffId;

	@JsonProperty("message")
	private String message;

	@JsonProperty("data")
	private List<MutasiData> data;
}
