
package com.project.app.custom.transaksi;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class MutasiRequest {

	@JsonProperty("norek")
	@NotNull
	private String norek;

	@JsonProperty("start_date")
	@NotNull
	private Long startDate;

	@JsonProperty("end_date")
	@NotNull
	private Long endDate;
}
