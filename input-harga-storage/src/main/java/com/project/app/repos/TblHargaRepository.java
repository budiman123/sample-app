package com.project.app.repos;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.project.app.domain.TblHarga;


public interface TblHargaRepository extends JpaRepository<TblHarga, Long> {
	Optional<TblHarga> findByStatusTrue();
	
	@Modifying
	@Query(value = "update tbl_harga set status = false", nativeQuery = true)
	void updateAllSetStatusFalse();
}
