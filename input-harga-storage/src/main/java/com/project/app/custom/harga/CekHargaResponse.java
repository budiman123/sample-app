
package com.project.app.custom.harga;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter 
@NoArgsConstructor
public class CekHargaResponse {
	@JsonProperty("error")
	private Boolean error;
		
	@JsonProperty("data")
	private Price data;
	
	@JsonProperty("message")
	private String message;
}
