
package com.project.app.custom.harga;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter 
@NoArgsConstructor
public class InputHargaRequest {
	@JsonProperty("admin_id")
	@NotNull
	private String adminId;
		
	@JsonProperty("harga_topup")
	@NotNull
	private Long hargaTopup;
	
	@JsonProperty("harga_buyback")
	@NotNull
	private Long hargaBuyback;
}
