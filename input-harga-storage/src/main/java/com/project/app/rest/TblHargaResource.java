package com.project.app.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.app.custom.harga.InputHargaRequest;
import com.project.app.custom.harga.InputHargaResponse;
import com.project.app.service.TblHargaService;

@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class TblHargaResource {

	private final TblHargaService tblHargaService;

	@Autowired
	public TblHargaResource(TblHargaService tblHargaService) {
		this.tblHargaService = tblHargaService;
	}

	@PostMapping("/input-harga")
	public ResponseEntity<InputHargaResponse> inputHarga(
			@RequestBody @Valid InputHargaRequest inputHargaRequest) {
		return ResponseEntity.ok(tblHargaService.doInputHarga(inputHargaRequest));
	}
}
