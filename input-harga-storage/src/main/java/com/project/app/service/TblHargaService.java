package com.project.app.service;

import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.app.custom.harga.InputHargaRequest;
import com.project.app.custom.harga.InputHargaResponse;
import com.project.app.domain.TblHarga;
import com.project.app.repos.TblHargaRepository;

@Service
public class TblHargaService {

	private final TblHargaRepository tblHargaRepository;

	@Autowired
	public TblHargaService(final TblHargaRepository tblHargaRepository) {
		this.tblHargaRepository = tblHargaRepository;
	}

	@Transactional
	public InputHargaResponse doInputHarga(InputHargaRequest inputHargaRequest) {
		InputHargaResponse response = new InputHargaResponse();
		response.setReffId(UUID.randomUUID().toString());
		response.setError(false);
		try {
			tblHargaRepository.updateAllSetStatusFalse();
			// create new record
			TblHarga harga = new TblHarga();
			harga.setCreatedBy(inputHargaRequest.getAdminId());
			harga.setStatus(true);
			harga.setHargaTopup(inputHargaRequest.getHargaTopup());
			harga.setHargaBuyback(inputHargaRequest.getHargaBuyback());
			tblHargaRepository.save(harga);

			// construct response
			return response;
		} catch (Exception e) {
			response.setError(true);
			response.setMessage(e.getMessage());
			return response;
		}
	}
}
