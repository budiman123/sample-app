
package com.project.app.custom.rekening;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter 
@NoArgsConstructor
public class UpdateSaldo {
	@JsonProperty("gram")
	private String gram;
	
	@JsonProperty("norek")
	private String norek;
	
	@JsonProperty("type")
	private String type;
}
