package com.project.app.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.project.app.custom.rekening.UpdateSaldo;
import com.project.app.domain.TblRekening;
import com.project.app.repos.TblRekeningRepository;

@Service
public class TblRekeningService {

	private final TblRekeningRepository tblRekeningRepository;

	public TblRekeningService(final TblRekeningRepository tblRekeningRepository) {
		this.tblRekeningRepository = tblRekeningRepository;
	}

	public TblRekening updateSaldoRekening(UpdateSaldo updateSaldo) {
		Optional<TblRekening> tblRekening = tblRekeningRepository.findById(updateSaldo.getNorek());
		if (!tblRekening.isPresent()) {
			TblRekening rekening = new TblRekening();
			rekening.setCreatedBy("system"); // since no auth set all by system
			rekening.setNorek(updateSaldo.getNorek());
			rekening.setCreatedDate(new Date());
			rekening.setSaldo(new BigDecimal(updateSaldo.getGram()));
			tblRekeningRepository.save(rekening);
			return rekening;
		} else {
			tblRekening.get().setUpdatedBy("system");
			tblRekening.get().setUpdatedDate(new Date());
			tblRekening.get().setSaldo(tblRekening.get().getSaldo().add(new BigDecimal(updateSaldo.getGram())));
			tblRekeningRepository.save(tblRekening.get());
			return tblRekening.get();
		}
	}
}
