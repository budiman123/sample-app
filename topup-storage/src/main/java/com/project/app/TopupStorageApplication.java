package com.project.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class TopupStorageApplication {

    public static void main(final String[] args) {
        SpringApplication.run(TopupStorageApplication.class, args);
    }

}
