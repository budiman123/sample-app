package com.project.app.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.app.domain.TblRekening;


public interface TblRekeningRepository extends JpaRepository<TblRekening, String> {

    boolean existsByNorekIgnoreCase(String norek);

}
