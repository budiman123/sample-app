
package com.project.app.custom.rekening;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter 
@NoArgsConstructor
public class CekSaldoResponse {
	@JsonProperty("error")
	private Boolean error;
		
	@JsonProperty("reff_id")
	private String reffId;
	
	@JsonProperty("message")
	private String message;
	
	@JsonProperty("data")
	private DetailRekening data;
}
