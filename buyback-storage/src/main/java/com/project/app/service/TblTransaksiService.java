package com.project.app.service;

import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.app.custom.rekening.UpdateSaldo;
import com.project.app.custom.transaction.InsertTransactionRequest;
import com.project.app.custom.transaction.TransactionResponse;
import com.project.app.domain.TblRekening;
import com.project.app.domain.TblTransaksi;
import com.project.app.repos.TblTransaksiRepository;

@Service
public class TblTransaksiService {

	private final TblTransaksiRepository tblTransaksiRepository;
	private final TblRekeningService tblRekeningService;

	@Autowired
	public TblTransaksiService(final TblTransaksiRepository tblTransaksiRepository, TblRekeningService tblRekeningService) {
		this.tblTransaksiRepository = tblTransaksiRepository;
		this.tblRekeningService = tblRekeningService;
	}

	@Transactional
	public TransactionResponse doTransaction(InsertTransactionRequest transactionRequest) {
		TransactionResponse response = new TransactionResponse();
		response.setReffId(UUID.randomUUID().toString());
		response.setError(false);
		try {
			TblTransaksi tblTransaksi = new TblTransaksi();
			tblTransaksi.setGram(transactionRequest.getGram());
			tblTransaksi.setHargaBuyback(transactionRequest.getHargaBuyback());
			tblTransaksi.setHargaTopup(transactionRequest.getHargaTopup());
			tblTransaksi.setNorek(transactionRequest.getNorek());
			tblTransaksi.setTimestamp(System.currentTimeMillis());
			tblTransaksi.setType(transactionRequest.getType());
			tblTransaksi.setCreatedBy("system");

			// update saldo
			UpdateSaldo updateSaldo = new UpdateSaldo();
			BeanUtils.copyProperties(transactionRequest, updateSaldo);
			updateSaldo.setGram(transactionRequest.getUpdatedGram());

			TblRekening tblRekening = tblRekeningService.updateSaldoRekening(updateSaldo);

			tblTransaksi.setSaldo(tblRekening.getSaldo());
			tblTransaksiRepository.save(tblTransaksi);

			return response;
		} catch (Exception e) {
			response.setError(true);
			response.setMessage(e.getMessage());
			return response;
		}
	}
}
