package com.project.app.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.app.custom.transaction.InsertTransactionRequest;
import com.project.app.custom.transaction.TransactionRequest;
import com.project.app.custom.transaction.TransactionResponse;
import com.project.app.service.TblTransaksiService;

@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class TblTransaksiResource {

	private final TblTransaksiService tblTransaksiService;

	@Autowired
	public TblTransaksiResource(final TblTransaksiService tblTransaksiService) {
		this.tblTransaksiService = tblTransaksiService;
	}

	@PostMapping("/insert-buyback")
	public ResponseEntity<TransactionResponse> doBuyback(@RequestBody @Valid InsertTransactionRequest insertTransactionRequest) {
		return ResponseEntity.ok(tblTransaksiService.doTransaction(insertTransactionRequest));
	}
}
