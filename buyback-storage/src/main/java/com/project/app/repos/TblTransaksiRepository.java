package com.project.app.repos;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.project.app.domain.TblTransaksi;


public interface TblTransaksiRepository extends JpaRepository<TblTransaksi, Long> {
	@Query(value = " SELECT * from tbl_transaksi  "
				 + " WHERE timestamp >= :start and timestamp <= :end "
				 + " and norek = :norek ",nativeQuery = true)
	List<TblTransaksi> getMutasiBetweenDateAndNorek(Long start, Long end, String norek);
	
}
