package com.project.app.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;


@Entity(name = "tbl_transaksi")
@Getter
@Setter
public class TblTransaksi {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(nullable = false, length = 10)
    private String type;

    @Column(nullable = false, precision = 1, scale = 3)
    private BigDecimal gram;

    @Column(nullable = false)
    private Long hargaTopup;

    @Column(nullable = false)
    private Long hargaBuyback;

    @Column(nullable = false, precision = 1, scale = 3)
    private BigDecimal saldo;

    @Column(nullable = false)
    private String norek;

    @Column(nullable = false)
    private Long timestamp;
    
    @Column(nullable = false)
    private String createdBy;
    
    @Column(nullable = false)
    private Date createdDate = new Date();

}
