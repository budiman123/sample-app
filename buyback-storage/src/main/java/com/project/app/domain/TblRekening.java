package com.project.app.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;


@Entity(name = "tbl_rekening")
@Getter
@Setter
public class TblRekening {

    @Id
    @Column(nullable = false, updatable = false, length = 10)
    private String norek;

    @Column(nullable = false, precision = 1, scale = 3)
    private BigDecimal saldo;

    @Column(nullable = false, length = 10)
    private String createdBy;

    @Column(nullable = false)
    private Date createdDate;

    @Column(length = 10)
    private String updatedBy;

    @Column
    private Date updatedDate;

}
