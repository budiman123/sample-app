--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.24
-- Dumped by pg_dump version 14.5 (Ubuntu 14.5-1.pgdg20.04+1)

-- Started on 2022-10-10 01:14:07 WIB

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 190 (class 1259 OID 22259)
-- Name: primary_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.primary_sequence
    START WITH 10000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.primary_sequence OWNER TO postgres;

SET default_tablespace = '';

--
-- TOC entry 186 (class 1259 OID 22215)
-- Name: tbl_harga; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tbl_harga (
    id bigint NOT NULL,
    created_by character varying(10) NOT NULL,
    harga_topup bigint NOT NULL,
    harga_buyback bigint NOT NULL,
    status boolean
);


ALTER TABLE public.tbl_harga OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 22213)
-- Name: tbl_harga_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tbl_harga_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tbl_harga_id_seq OWNER TO postgres;

--
-- TOC entry 2153 (class 0 OID 0)
-- Dependencies: 185
-- Name: tbl_harga_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tbl_harga_id_seq OWNED BY public.tbl_harga.id;


--
-- TOC entry 189 (class 1259 OID 22232)
-- Name: tbl_rekening; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tbl_rekening (
    norek character varying NOT NULL,
    saldo numeric NOT NULL,
    created_by character varying NOT NULL,
    created_date timestamp without time zone NOT NULL,
    updated_by character varying,
    updated_date timestamp without time zone
);


ALTER TABLE public.tbl_rekening OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 22223)
-- Name: tbl_transaksi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tbl_transaksi (
    id bigint NOT NULL,
    type character varying NOT NULL,
    gram numeric NOT NULL,
    harga_topup bigint NOT NULL,
    harga_buyback bigint NOT NULL,
    saldo numeric NOT NULL,
    norek character varying(10) NOT NULL,
    "timestamp" bigint NOT NULL
);


ALTER TABLE public.tbl_transaksi OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 22221)
-- Name: tbl_transaksi_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tbl_transaksi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tbl_transaksi_id_seq OWNER TO postgres;

--
-- TOC entry 2154 (class 0 OID 0)
-- Dependencies: 187
-- Name: tbl_transaksi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tbl_transaksi_id_seq OWNED BY public.tbl_transaksi.id;


--
-- TOC entry 2017 (class 2604 OID 22218)
-- Name: tbl_harga id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tbl_harga ALTER COLUMN id SET DEFAULT nextval('public.tbl_harga_id_seq'::regclass);


--
-- TOC entry 2018 (class 2604 OID 22226)
-- Name: tbl_transaksi id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tbl_transaksi ALTER COLUMN id SET DEFAULT nextval('public.tbl_transaksi_id_seq'::regclass);


--
-- TOC entry 2143 (class 0 OID 22215)
-- Dependencies: 186
-- Data for Name: tbl_harga; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tbl_harga (id, created_by, harga_topup, harga_buyback, status) FROM stdin;
1	101	10000	9000	t
\.


--
-- TOC entry 2146 (class 0 OID 22232)
-- Dependencies: 189
-- Data for Name: tbl_rekening; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tbl_rekening (norek, saldo, created_by, created_date, updated_by, updated_date) FROM stdin;
\.


--
-- TOC entry 2145 (class 0 OID 22223)
-- Dependencies: 188
-- Data for Name: tbl_transaksi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tbl_transaksi (id, type, gram, harga_topup, harga_buyback, saldo, norek, "timestamp") FROM stdin;
\.


--
-- TOC entry 2155 (class 0 OID 0)
-- Dependencies: 190
-- Name: primary_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.primary_sequence', 10000, false);


--
-- TOC entry 2156 (class 0 OID 0)
-- Dependencies: 185
-- Name: tbl_harga_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tbl_harga_id_seq', 1, true);


--
-- TOC entry 2157 (class 0 OID 0)
-- Dependencies: 187
-- Name: tbl_transaksi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tbl_transaksi_id_seq', 1, false);


--
-- TOC entry 2020 (class 2606 OID 22220)
-- Name: tbl_harga tbl_harga_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tbl_harga
    ADD CONSTRAINT tbl_harga_pkey PRIMARY KEY (id);


--
-- TOC entry 2024 (class 2606 OID 22239)
-- Name: tbl_rekening tbl_rekening_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tbl_rekening
    ADD CONSTRAINT tbl_rekening_pkey PRIMARY KEY (norek);


--
-- TOC entry 2022 (class 2606 OID 22231)
-- Name: tbl_transaksi tbl_transaksi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tbl_transaksi
    ADD CONSTRAINT tbl_transaksi_pkey PRIMARY KEY (id);


-- Completed on 2022-10-10 01:14:09 WIB

--
-- PostgreSQL database dump complete
--

