package com.project.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;


@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class BuybackServiceApplication {

    public static void main(final String[] args) {
        SpringApplication.run(BuybackServiceApplication.class, args);
    }

}
