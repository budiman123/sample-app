package com.project.app.service;

import java.math.BigDecimal;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.project.app.client.BuybackStorageClient;
import com.project.app.client.CheckHargaClient;
import com.project.app.custom.harga.CekHargaResponse;
import com.project.app.custom.rekening.CekSaldoRequest;
import com.project.app.custom.rekening.CekSaldoResponse;
import com.project.app.custom.transaction.InsertTransactionRequest;
import com.project.app.custom.transaction.TransactionRequest;
import com.project.app.custom.transaction.TransactionResponse;

@Service
public class TblTransaksiService {

	private final ObjectMapper objectMapper = new ObjectMapper().disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
	
	private final CheckHargaClient checkHargaClient;
	private final BuybackStorageClient buybackStorageClient;
	private final Gson gson = new Gson();
	
	@Value("${buyback.url}")
	private String BUYBACK_URL;
	
	@Value("${check-saldo.url}")
	private String CHECK_SALDO_URL;

	@Autowired
	public TblTransaksiService(
			CheckHargaClient checkHargaClient, 
			BuybackStorageClient buybackStorageClient) {
		this.checkHargaClient = checkHargaClient;
		this.buybackStorageClient = buybackStorageClient;
	}

	public TransactionResponse doTransaction(TransactionRequest transactionRequest) {
		TransactionResponse response = new TransactionResponse();
		response.setReffId(UUID.randomUUID().toString());
		response.setError(false);
		try {

			CekSaldoRequest request = new CekSaldoRequest();
			request.setNorek(transactionRequest.getNorek());
			
			// call to buyback storage service to get saldo
			String result = buybackStorageClient.doHttpGet(gson.toJson(request), CHECK_SALDO_URL);
			CekSaldoResponse cekSaldoResponse = objectMapper.readValue(result, CekSaldoResponse.class);
			if(cekSaldoResponse.getError()) {
				throw new Exception(cekSaldoResponse.getMessage());
			}
			
			CekHargaResponse cekHargaResponse = validateBuyback(transactionRequest.getGram(), transactionRequest.getHarga(),
					cekSaldoResponse);
			// create new record
			InsertTransactionRequest insertTransactionRequest = new InsertTransactionRequest();
			insertTransactionRequest.setGram(new BigDecimal(transactionRequest.getGram()));
			insertTransactionRequest.setHargaBuyback(cekHargaResponse.getData().getHargaBuyback());
			insertTransactionRequest.setHargaTopup(cekHargaResponse.getData().getHargaTopup());
			insertTransactionRequest.setNorek(transactionRequest.getNorek());
			insertTransactionRequest.setTimestamp(System.currentTimeMillis());
			insertTransactionRequest.setType(transactionRequest.getType());
			insertTransactionRequest.setUpdatedGram("-"+transactionRequest.getGram());
			buybackStorageClient.doHttpPost(gson.toJson(insertTransactionRequest), BUYBACK_URL);

			return response;
		} catch (Exception e) {
			response.setError(true);
			response.setMessage(e.getMessage());
			return response;
		}
	}

	private CekHargaResponse validateBuyback(String gram, Long harga, CekSaldoResponse cekSaldoResponse) throws Exception {
		if (cekSaldoResponse.getData().getSaldo().compareTo(new BigDecimal(0)) <= 0) {
			throw new Exception("You dont have any saldo");
		}

		if (cekSaldoResponse.getData().getSaldo().compareTo(new BigDecimal(gram)) < 0) {
			throw new Exception("Insufficient saldo");
		}

		// find current harga
		CekHargaResponse cekHargaResponse = checkHargaClient.checkHarga();

		if (cekHargaResponse.getData().getHargaBuyback() > harga & cekHargaResponse.getData().getHargaBuyback() < harga) {
			throw new Exception("Harga buyback do not match");
		}
		return cekHargaResponse;
	}
}
