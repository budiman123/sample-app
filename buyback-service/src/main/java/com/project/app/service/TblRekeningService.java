package com.project.app.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.project.app.custom.rekening.CekSaldoRequest;
import com.project.app.custom.rekening.CekSaldoResponse;
import com.project.app.custom.rekening.DetailRekening;
import com.project.app.custom.rekening.UpdateSaldo;
import com.project.app.domain.TblRekening;
import com.project.app.repos.TblRekeningRepository;

@Service
public class TblRekeningService {

	private final TblRekeningRepository tblRekeningRepository;

	public TblRekeningService(final TblRekeningRepository tblRekeningRepository) {
		this.tblRekeningRepository = tblRekeningRepository;
	}

	public TblRekening updateSaldoRekening(UpdateSaldo updateSaldo) {
		Optional<TblRekening> tblRekening = tblRekeningRepository.findById(updateSaldo.getNorek().toLowerCase());
		if (!tblRekening.isPresent()) {
			TblRekening rekening = new TblRekening();
			rekening.setCreatedBy("system"); // since no auth set all by system
			rekening.setNorek(updateSaldo.getNorek());
			rekening.setCreatedDate(new Date());
			rekening.setSaldo(new BigDecimal(updateSaldo.getGram()));
			tblRekeningRepository.save(rekening);
			return rekening;
		} else {
			tblRekening.get().setUpdatedBy("system");
			tblRekening.get().setUpdatedDate(new Date());
			tblRekening.get().setSaldo(tblRekening.get().getSaldo().add(new BigDecimal(updateSaldo.getGram())));
			tblRekeningRepository.save(tblRekening.get());
			return tblRekening.get();
		}
	}

	public CekSaldoResponse getSaldo(CekSaldoRequest request) {
		CekSaldoResponse response = new CekSaldoResponse();
		response.setError(false);
		response.setReffId(UUID.randomUUID().toString());
		try {
			Optional<TblRekening> tblRekening = tblRekeningRepository.findById(request.getNorek().toLowerCase());
			if (!tblRekening.isPresent()) {
				throw new Exception("No rek not found or you dont have balance yet");
			}
			DetailRekening detailRekening = new DetailRekening();
			detailRekening.setNorek(tblRekening.get().getNorek());
			detailRekening.setSaldo(tblRekening.get().getSaldo());
			response.setData(detailRekening);
			return response;
		} catch (Exception e) {
			response.setMessage(e.getMessage());
			return response;
		}
	}
}
