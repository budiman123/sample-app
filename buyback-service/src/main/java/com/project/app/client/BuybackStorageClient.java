package com.project.app.client;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@Service
public class BuybackStorageClient {

	public String doHttpGet(String requestBody, String url) throws Exception {
		OkHttpClient client = new OkHttpClient().newBuilder()
				  .build();
				MediaType mediaType = MediaType.parse("application/json");
				RequestBody body = RequestBody.create(mediaType, "{\n    \"norek\": \"123\"\n}");
				Request request = new Request.Builder()
				  .url(url)
				  .method("POST", body)
				  .addHeader("Content-Type", "application/json")
				  .build();
				Response response = client.newCall(request).execute();

		return response.body().string();
	}
	

	public String doHttpPost(String requestBody, String url) throws Exception {
		OkHttpClient client = new OkHttpClient().newBuilder().build();

		RequestBody body = null;
		if (requestBody != null) {
			MediaType mediaType = MediaType.parse("application/json");
			body = RequestBody.create(mediaType, requestBody);
		}

		Request request = new Request.Builder().url(url).method("POST", body).build();
		Response response = client.newCall(request).execute();
		if(response.code() != HttpStatus.OK.value()) {
			throw new Exception("Something went wrong when insert data");
		}
		return response.body().string();
	}
}
