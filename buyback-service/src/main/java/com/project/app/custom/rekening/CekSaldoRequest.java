
package com.project.app.custom.rekening;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter 
@NoArgsConstructor
public class CekSaldoRequest {
	@JsonProperty("norek")
	private String norek;
}
