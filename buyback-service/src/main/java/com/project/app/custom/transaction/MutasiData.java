
package com.project.app.custom.transaction;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class MutasiData {
	@JsonProperty("date")
	private Long date;

	@JsonProperty("type")
	private String type;

	@JsonProperty("gram")
	private BigDecimal gram;

	@JsonProperty("harga_topup")
	private Long hargaTopup;

	@JsonProperty("harga_buyback")
	private Long hargaBuyback;

	@JsonProperty("saldo")
	private BigDecimal saldo;
}
