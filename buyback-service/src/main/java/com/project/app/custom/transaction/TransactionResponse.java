
package com.project.app.custom.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class TransactionResponse {
	@JsonProperty("error")
	private Boolean error;

	@JsonProperty("reff_id")
	private String reffId;

	@JsonProperty("message")
	private String message;

	@JsonProperty("data")
	private SaldoData data;
}
