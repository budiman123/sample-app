package com.project.app.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;


@Entity(name = "tbl_harga")
@Getter
@Setter
public class TblHarga {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(nullable = false, length = 10)
    private String createdBy;

    @Column(nullable = false)
    private Long hargaTopup;

    @Column(nullable = false)
    private Long hargaBuyback;

    @Column(nullable = false)
    private Boolean status;

}
