package com.project.app.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.app.custom.harga.CekHargaResponse;
import com.project.app.service.TblHargaService;

@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class TblHargaResource {

	private final TblHargaService tblHargaService;

	@Autowired
	public TblHargaResource(TblHargaService tblHargaService) {
		this.tblHargaService = tblHargaService;
	}

	@GetMapping("/check-harga")
	public ResponseEntity<CekHargaResponse> doCheckHarga() {
		return ResponseEntity.ok(tblHargaService.doCheckResponse());
	}
}
