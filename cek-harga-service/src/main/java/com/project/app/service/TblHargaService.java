package com.project.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.app.custom.harga.CekHargaResponse;
import com.project.app.custom.harga.Price;
import com.project.app.domain.TblHarga;
import com.project.app.repos.TblHargaRepository;

@Service
public class TblHargaService {

	private final TblHargaRepository tblHargaRepository;

	@Autowired
	public TblHargaService(final TblHargaRepository tblHargaRepository) {
		this.tblHargaRepository = tblHargaRepository;
	}

	public CekHargaResponse doCheckResponse() {
		Optional<TblHarga> tblHarga = tblHargaRepository.findByStatusTrue();
		CekHargaResponse response = new CekHargaResponse();
		if (!tblHarga.isPresent()) {
			response.setData(null);
			response.setError(true);
			response.setMessage("Data not found");
			return response;
		}
		response.setData(constructPrice(tblHarga.get()));
		response.setError(false);
		return response;
	}
	
	private Price constructPrice(TblHarga tblHarga) {
		Price price = new Price();
		price.setHargaBuyback(tblHarga.getHargaBuyback());
		price.setHargaTopup(tblHarga.getHargaTopup());
		return price;
	}
}
