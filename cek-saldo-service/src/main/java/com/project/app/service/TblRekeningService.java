package com.project.app.service;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.project.app.custom.rekening.CekSaldoRequest;
import com.project.app.custom.rekening.CekSaldoResponse;
import com.project.app.custom.rekening.DetailRekening;
import com.project.app.repos.TblRekeningRepository;

@Service
public class TblRekeningService {

	private final TblRekeningRepository tblRekeningRepository;

	public TblRekeningService(final TblRekeningRepository tblRekeningRepository) {
		this.tblRekeningRepository = tblRekeningRepository;
	}

	public CekSaldoResponse getSaldo(CekSaldoRequest request) {
		CekSaldoResponse response = new CekSaldoResponse();
		response.setError(false);
		response.setReffId(UUID.randomUUID().toString());
		try {
			Optional<Map<String, Object>> tblRekening = tblRekeningRepository.getRekeningByNorek(request.getNorek());
			if (!tblRekening.isPresent()) {
				throw new Exception("No rek not found or you dont have balance yet");
			}
			DetailRekening detailRekening = new DetailRekening();
			detailRekening.setNorek(tblRekening.get().get("norek").toString());
			detailRekening.setSaldo(new BigDecimal(tblRekening.get().get("saldo").toString()));
			response.setData(detailRekening);
			return response;
		} catch (Exception e) {
			response.setError(true);
			response.setMessage(e.getMessage());
			return response;
		}
	}
}
