package com.project.app.rest;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.app.custom.rekening.CekSaldoRequest;
import com.project.app.custom.rekening.CekSaldoResponse;
import com.project.app.service.TblRekeningService;


@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class TblRekeningResource {

    private final TblRekeningService tblRekeningService;

    public TblRekeningResource(final TblRekeningService tblRekeningService) {
        this.tblRekeningService = tblRekeningService;
    }

    @RequestMapping("/saldo")
    public ResponseEntity<CekSaldoResponse> getSaldo(@RequestBody @Valid CekSaldoRequest cekSaldoRequest) {
        return ResponseEntity.ok(tblRekeningService.getSaldo(cekSaldoRequest));
    }
}
