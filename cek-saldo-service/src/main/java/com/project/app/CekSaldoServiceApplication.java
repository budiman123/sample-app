package com.project.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class CekSaldoServiceApplication {

    public static void main(final String[] args) {
        SpringApplication.run(CekSaldoServiceApplication.class, args);
    }

}
