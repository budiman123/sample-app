package com.project.app.repos;

import java.util.Map;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.project.app.domain.TblRekening;


public interface TblRekeningRepository extends JpaRepository<TblRekening, String> {

    boolean existsByNorekIgnoreCase(String norek);

    @Query(value = "Select norek, saldo from tbl_rekening where norek = :norek", nativeQuery=true)
    Optional<Map<String, Object>> getRekeningByNorek(String norek);
}
